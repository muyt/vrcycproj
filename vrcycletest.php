<?php
require_once 'vendor\goat1000\svggraph\autoloader.php';

$graph = new Goat1000\SVGGraph\SVGGraph(400, 300);
$settings = [
    'auto_fit' => true,
    'back_colour' => '#eee',
    'back_stroke_width' => 0,
    'back_stroke_colour' => '#eee',
    'stroke_colour' => '#000',
    'axis_colour' => '#333',
    'axis_overlap' => 2,
    'grid_colour' => '#666',
    'label_colour' => '#000',
    'axis_font' => 'Arial',
    'axis_font_size' => 10,
    'pad_right' => 20,
    'pad_left' => 20,
    'marker_type' => ['circle','square'],
    'marker_size' => 3,
    'marker_colour' => ['blue','red'],
    'fill_under' => true,
    'link_base' => '/',
    'link_target' => '_top',
    'show_labels' => true,
    'label_font' => 'Arial',
    'label_font_size' => '11',
    'minimum_grid_spacing' => 20,
    'show_subdivisions' => true,
    'show_grid_subdivisions' => true,
    'grid_subdivision_colour' => '#ccc',
  ];
?>
<!DOCTYPE html>
 <html>
 <head>
  <title>SVGGraph example</title>
 </head>
 <body>
  <h1>Example of SVG in HTML5</h1>
  <div>
<?php

$graph->values(10, 14, 6, 3, 20, 14, 16);
echo $graph->fetch('BarGraph', false);

?>
  </div>
  <div>
<?php

$graph->values(8, 15, 14, 19, 12, 15, 13);
echo $graph->fetch('BarGraph', false);

?>
<?php
$values = array(
    array('Spacial Awareness' => 30, 'Reaction Time' => 50,
      'Planning and Decision' => 25, 'Memory' => 45, 'Visual Perception' => 35),
   // array('Spacial Awareness' => 20, 'Reaction Time' => 30, 'Me' => 20,
     // 'Planning and decision' => 15, 'Memory' => 25, 'Visual Perception' => 35, 'Tea' => 45)
  );
  
  $colours = array(
    array('red', 'yellow'), array('blue', 'white')
  );
//   $links = array(
//     'Spacial Awareness' => 'jpegsaver.php', 'Reaction Time' => 'crcdropper.php',
//     'Me' => 'svggraph.php'
//   );
   
  $graph = new Goat1000\SVGGraph\SVGGraph(400, 400, $settings);
  
  $graph->colours($colours);
  $graph->values($values);
  //$graph->links($links);
  $graph->render('MultiRadarGraph');


?>



  </div>
<?php echo $graph->fetchJavascript(); ?>
 </body>
 </html>