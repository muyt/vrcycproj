<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'type' => 'lib',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => 'goats1000/svggraph',
        'dev' => true,
    ),
    'versions' => array(
        'goat1000/svggraph' => array(
            'pretty_version' => '3.10.0',
            'version' => '3.10.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../goat1000/svggraph',
            'aliases' => array(),
            'reference' => 'e97249ce511b20650b82e4df178c0ec2ea4db20c',
            'dev_requirement' => false,
        ),
        'goats1000/svggraph' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'type' => 'lib',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
    ),
);
